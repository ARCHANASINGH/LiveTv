package com.robosoft.livetv.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelections;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.robosoft.livetv.Network.NetworkStatus;
import com.robosoft.livetv.R;
import com.robosoft.livetv.Util.Constants;
import com.robosoft.livetv.Util.EventLogger;
import com.robosoft.livetv.Util.LiveTVUtil;
import com.robosoft.livetv.Widget.VideoControllerView;
import com.robosoft.livetv.application.LiveTvApplication;

@TargetApi(16)
public class ExoPlayerActivity extends Activity implements TrackSelector.EventListener<MappingTrackSelector.MappedTrackInfo>, ExoPlayer.EventListener, SurfaceHolder.Callback, VideoControllerView.MediaPlayerControl, SimpleExoPlayer.VideoListener {

    private static final String TAG = "ExoPlayerActivity";
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    private boolean mIsShouldAutoPlay;
    private boolean mShouldNeedSource;
    private boolean mFlag;

    private MappingTrackSelector mTrackSelector;
    private EventLogger mEventLogger;
    private Handler mHandler;

    private SimpleExoPlayer player;
    private VideoControllerView mControllerView;
    private FrameLayout mFrameLyt;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        //Initialize Ui
        initUi();
        //Initialize Player
        initPlayer();
        //Prepare Player
        preparePlayer();
        //Initialize surfaceView
        initSurfaceView();
    }

    private void initUi() {
        mFrameLyt = (FrameLayout) findViewById(R.id.container);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        mControllerView = new VideoControllerView(this);
    }

    private void initSurfaceView() {
        //Create surface to render vedio
        SurfaceView vedioSurface = (SurfaceView) findViewById(R.id.video_surface);
        SurfaceHolder videoHolder = vedioSurface.getHolder();
        videoHolder.addCallback(this);
    }

    private void initPlayer() {
        if (player == null) {
            // handler for dispatching event that is trigger in player
            mHandler = new Handler();
            //event
            mEventLogger = new EventLogger();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(BANDWIDTH_METER);
            //track selection
            mTrackSelector = new DefaultTrackSelector(mHandler, videoTrackSelectionFactory);
            mTrackSelector.addListener(this);
            mTrackSelector.addListener(mEventLogger);
            // create the player instance
            player = ExoPlayerFactory.newSimpleInstance(this, mTrackSelector, new DefaultLoadControl());

        }
    }

    private void preparePlayer() {

        if (URLUtil.isValidUrl(Constants.URL)) {
            mProgressBar.setVisibility(View.VISIBLE);
            if (NetworkStatus.isNetworkAvailable(this)) {
                // create mediaSource i.e hls media source
                HlsMediaSource hlsMediaSource = new HlsMediaSource(Uri.parse(Constants.URL), buildDataSourceFactory(), mHandler, mEventLogger);
                // player.addListener(eventLogger);
                player.setVideoListener(this);
                player.addListener(this);
                // prepare the player
                player.prepare(hlsMediaSource);
                // make ready to play
                player.setPlayWhenReady(!mIsShouldAutoPlay);
                //set controller
                mControllerView.setMediaPlayer(this);
                mControllerView.setAnchorView(mFrameLyt);
                // show mediaController
                if (player.getPlayWhenReady()) {
                    mControllerView.show();
                }
            } else {
                mProgressBar.setVisibility(View.GONE);
                if (!isFinishing()) {
                    showDialog(getString(R.string.network_error));
                }
            }
        }
    }


    private void showDialog(final String msg) {
        LiveTVUtil.showDialog(this, msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (NetworkStatus.isNetworkAvailable(ExoPlayerActivity.this)) {
                    preparePlayer();
                } else {
                    if (!isFinishing()) {
                        showDialog(msg);
                    }
                }

            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    toggleControllerOnTapOf();
                    // Check whether statusbar is show or hidden
                    if (getWindow() != null && getWindow().getDecorView() != null) {
                        if (getWindow().getDecorView().getSystemUiVisibility() == 0) {
                            LiveTVUtil.hideStatusBarOnClick(this);
                        } else {
                            LiveTVUtil.showStatusBarOnClick(this);
                        }
                    }

                } else {
                    toggleControllerOnTapOf();
                }
        }
        return false;
    }

    private boolean toggleControllerOnTapOf() {
        if (mControllerView.isShowing()) {
            mControllerView.hide();
            return true;

        } else {
            mControllerView.show();
            return true;
        }
    }

    /**
     * Returns a new DataSource factory.
     *
     * @return A new DataSource factory.
     */
    private DataSource.Factory buildDataSourceFactory() {
        return ((LiveTvApplication) getApplication()).buildDataSourceFactory(BANDWIDTH_METER);
    }

    @Override
    public void onTrackSelectionsChanged(TrackSelections<? extends MappingTrackSelector.MappedTrackInfo> trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    //// TODO: 27/11/16
    @Override
    public void onPlayerError(ExoPlaybackException error) {
    }

    @Override
    public void onPositionDiscontinuity() {

    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (player != null) {
            player.setVideoSurface(surfaceHolder.getSurface());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (player != null) {
            player.setVideoSurface(null);
        }
    }


    @Override
    public void start() {
        if (player != null) {
            player.setPlayWhenReady(!mIsShouldAutoPlay);
        }
    }

    @Override
    public void pause() {
        if (isPlaying()) {
            player.setPlayWhenReady(mIsShouldAutoPlay);
        }
    }

    @Override
    public boolean isPlaying() {
        return player.getPlayWhenReady();
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean isFullScreen() {
        return mFlag;
    }

    @Override
    public void toggleOrientation(int orientation) {
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            LiveTVUtil.showStatusBarOnClick(this);
            LiveTVUtil.expandVedio(orientation, mFrameLyt);
            mFlag = false;
        } else if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            LiveTVUtil.hideStatusBarOnClick(this);
            LiveTVUtil.expandVedio(orientation, mFrameLyt);
            mFlag = true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (player != null) {
            player.release();
        }
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

    }

    @Override
    public void onRenderedFirstFrame() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onVideoTracksDisabled() {
    }
}
