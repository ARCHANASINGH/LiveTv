package com.robosoft.livetv.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.robosoft.livetv.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUi();
    }
    // initialize views
    private void initUi() {
        Button playBtn = (Button) findViewById(R.id.exoplay_btn);
        playBtn.setOnClickListener(this);
        Button mediaPlayerBtn = (Button) findViewById(R.id.media_play_btn);
        mediaPlayerBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            // Go to ExoPlayerActivity
            case R.id.exoplay_btn:
                Intent intent = new Intent(this, ExoPlayerActivity.class);
                startActivity(intent);
                break;
            // Go to MediaPlayerActivity
            case R.id.media_play_btn:
                Intent mpIntent = new Intent(this, MediaPlayerActivity.class);
                startActivity(mpIntent);
                break;
        }

    }
}
