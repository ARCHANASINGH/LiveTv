package com.robosoft.livetv.Activity;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.robosoft.livetv.Network.NetworkStatus;
import com.robosoft.livetv.R;
import com.robosoft.livetv.Util.Constants;
import com.robosoft.livetv.Util.LiveTVUtil;
import com.robosoft.livetv.Widget.VideoControllerView;

import java.io.IOException;

public class MediaPlayerActivity extends AppCompatActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, VideoControllerView.MediaPlayerControl {

    private static final int CONTAINER_HEIGHT = 900;
    private static final int SHOW_ERROR_DIALOG = 2;
    private static final int SHOW_NETWORK_ERROR_DIALOG = 1;

    private boolean mFlag;

    private MediaPlayer mPlayer;
    private VideoControllerView mControllerView;
    private ProgressBar mProgressBar;
    private FrameLayout mFrameLyt;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        //initialize Views
        initUi();
        //initialize Player
        initPlayer();
        //set DataSource
        setPlayerDataSource();
        //initialize serfaceView
        initSerfaceView();
    }

    private void initUi() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        mFrameLyt = (FrameLayout) findViewById(R.id.container);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        mControllerView = new VideoControllerView(this);
    }

    private void initSerfaceView() {
        SurfaceView videoSurface = (SurfaceView) findViewById(R.id.video_surface);
        //access serfaceView
        SurfaceHolder videoHolder = videoSurface.getHolder();
        videoHolder.addCallback(this);
    }

    private void initPlayer() {
        if (mPlayer == null) {
            //idle state
            mPlayer = new MediaPlayer();
            mPlayer.setOnPreparedListener(this);
            mPlayer.setOnErrorListener(this);

        }
    }

    private void setPlayerDataSource() {
        try {
            if (NetworkStatus.isNetworkAvailable(this)) {
                if (URLUtil.isValidUrl(Constants.URL)) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    if (mPlayer != null) {
                        //initialized state
                        mPlayer.setDataSource(this, Uri.parse(Constants.URL));
                        mPlayer.prepareAsync();
                    }
                }
            } else {
                if (!isFinishing()) {
                    showDialog(getString(R.string.network_error), SHOW_NETWORK_ERROR_DIALOG);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        mPlayer.setDisplay(surfaceHolder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mControllerView.setMediaPlayer(this);
        mControllerView.setAnchorView((FrameLayout) findViewById(R.id.container));
        mPlayer.start();
        if (mPlayer.isPlaying()) {
            mProgressBar.setVisibility(View.GONE);
            mControllerView.show();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    toggleControllerOnTapOf();
                    // Check whether statusbar is show or hidden
                    if (getWindow() != null && getWindow().getDecorView() != null) {
                        if (getWindow().getDecorView().getSystemUiVisibility() == 0) {
                            LiveTVUtil.hideStatusBarOnClick(this);
                        } else {
                            LiveTVUtil.showStatusBarOnClick(this);
                        }
                    }

                } else {
                    toggleControllerOnTapOf();
                }

        }
        return false;
    }

    private boolean toggleControllerOnTapOf() {
        if (mControllerView.isShowing()) {
            mControllerView.hide();
            return true;
        } else {
            mControllerView.show();
            return true;
        }
    }

    @Override
    public void start() {
        if (mPlayer != null && !mPlayer.isPlaying()) {
            mPlayer.start();
        }
    }

    @Override
    public void pause() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();
        }
    }

    @Override
    public boolean isPlaying() {
        if (mPlayer != null) {
            return mPlayer.isPlaying();
        }
        return false;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean isFullScreen() {
        return mFlag;
    }

    @Override
    public void toggleOrientation(int orientation) {
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            LiveTVUtil.showStatusBarOnClick(this);
            expandVedio(orientation);
            mFlag = false;
        } else if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            LiveTVUtil.hideStatusBarOnClick(this);
            expandVedio(orientation);
            mFlag = true;
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        mPlayer = mediaPlayer;
        if (!isFinishing()) {
            showDialog(getError(what, extra), SHOW_ERROR_DIALOG);
        }
        return true;
    }

    private String getError(int what, int extra) {
        String msg = "";
        if (what == MediaPlayer.MEDIA_ERROR_UNKNOWN) {
            msg = getErrorDetails(getString(R.string.media_type_of_error_unknown), extra);
            return msg;
        } else if (what == MediaPlayer.MEDIA_ERROR_SERVER_DIED) {
            msg = getErrorDetails(getString(R.string.media_type_of_error_server_died), extra);
            return msg;
        }
        return "";
    }

    private String getErrorDetails(String msg, int extra) {
        if (extra == MediaPlayer.MEDIA_ERROR_IO) {
            return msg + getString(R.string.new_line) + getString(R.string.media_extra_media_io);
        } else if (extra == MediaPlayer.MEDIA_ERROR_TIMED_OUT) {
            return msg = msg + getString(R.string.new_line) + getString(R.string.media_extra_error_timed_out);
        } else if (extra == MediaPlayer.MEDIA_ERROR_UNSUPPORTED) {
            return msg = msg + getString(R.string.new_line) + getString(R.string.media_extra_unsupported);
        } else if (extra == MediaPlayer.MEDIA_ERROR_MALFORMED) {
            return msg = msg + getString(R.string.new_line) + getString(R.string.media_extra_malformed);
        }
        return msg;
    }


    private void showDialog(final String msg, final int type) {
        LiveTVUtil.showDialog(this, msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (NetworkStatus.isNetworkAvailable(MediaPlayerActivity.this)) {
                    switch (type) {
                        case SHOW_ERROR_DIALOG:
                            mPlayer.reset();
                            setPlayerDataSource();
                            mPlayer.setOnPreparedListener(MediaPlayerActivity.this);
                            mPlayer.setOnErrorListener(MediaPlayerActivity.this);
                            break;
                        case SHOW_NETWORK_ERROR_DIALOG:
                            setPlayerDataSource();
                            break;
                    }
                } else {
                    if (!isFinishing()) {
                        showDialog(msg, type);
                    }
                }
            }
        });
    }

    private void expandVedio(int orientaion) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mFrameLyt.getLayoutParams();
        if (orientaion == Configuration.ORIENTATION_LANDSCAPE) {
            setParams(params, CONTAINER_HEIGHT, params.MATCH_PARENT);
        } else {
            setParams(params, params.MATCH_PARENT, params.WRAP_CONTENT);
        }
    }

    private void setParams(LinearLayout.LayoutParams params, int height, int width) {
        params.height = height;
        params.width = width;
        mFrameLyt.setLayoutParams(params);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPlayer != null) {
            mPlayer.release();
        }
    }
}

