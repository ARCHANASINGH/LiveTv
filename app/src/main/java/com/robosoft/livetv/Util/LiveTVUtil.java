package com.robosoft.livetv.Util;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.robosoft.livetv.R;

/**
 * Created by archana on 15/11/16.
 */
public class LiveTVUtil {
    public static void showDialog(Activity act, String msg, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder alert = new AlertDialog.Builder(act);
        alert.setMessage(msg);
        alert.setPositiveButton(act.getString(R.string.retry), listener);
        alert.create();
        alert.show();
    }

    public static void hideStatusBarOnClick(Activity act) {
        if (act.getWindow() != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                act.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                View decorView = act.getWindow().getDecorView();
                if (decorView != null) {
                    // hide the status bar
                    Log.i("Hello", "Hide *********************");
                    int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                    decorView.setSystemUiVisibility(uiOptions);
                }

            }
        }
    }

    public static void showStatusBarOnClick(Activity act) {
        if (act.getWindow() != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                act.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                View decorView = act.getWindow().getDecorView();
                if (decorView != null) {
                    // show the status bar
                    int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
                    decorView.setSystemUiVisibility(uiOptions);
                }

            }
        }
    }

    public static void expandVedio(int orientaion , FrameLayout mFrameLyt) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mFrameLyt.getLayoutParams();
        if (orientaion == Configuration.ORIENTATION_LANDSCAPE) {
            setParams(params, Constants.CONTAINER_HEIGHT, params.MATCH_PARENT,mFrameLyt);
        } else {
            setParams(params, params.MATCH_PARENT, params.WRAP_CONTENT,mFrameLyt);
        }
    }

    private static void setParams(LinearLayout.LayoutParams params, int height, int width,FrameLayout mFrameLyt) {
        params.height = height;
        params.width = width;
        mFrameLyt.setLayoutParams(params);
    }


}
