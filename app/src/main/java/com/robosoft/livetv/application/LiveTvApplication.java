package com.robosoft.livetv.application;

import android.app.Application;

import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.robosoft.livetv.R;

/**
 * Created by archana on 1/11/16.
 */
public class LiveTvApplication extends Application {

    private String mUserAgent;

    @Override
    public void onCreate() {
        super.onCreate();
        getmUserAgent();
    }

    private String getmUserAgent(){
        mUserAgent = Util.getUserAgent(this,getApplicationContext().getString(R.string.app_name) );
        return mUserAgent;
    }

    public DataSource.Factory buildDataSourceFactory(DefaultBandwidthMeter bandwidthMeter){
       return new DefaultDataSourceFactory(this,bandwidthMeter,buildHttpDataSourceFactory(bandwidthMeter));
    }

    //// TODO: 25/11/16
    public HttpDataSource.Factory buildHttpDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultHttpDataSourceFactory(getmUserAgent(), bandwidthMeter);
    }
}
